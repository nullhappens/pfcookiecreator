/*globals requirejs, require*/
requirejs.config({
	baseUrl: './js',
	paths: {
		//vendor libraries
		jquery: 'lib/jquery-2.1.4',
		underscore: 'lib/underscore',
		backbone: 'lib/backbone',
		marionette: 'lib/backbone.marionette',
		mustache: 'lib/mustache',
		text: 'lib/text',
		stache: 'lib/stache',

		//Application specific
		app: 'application',
		models: 'application/models',
		views: 'application/views',
		controllers: 'application/controllers',
		templates: 'application/templates'
	}
});

require([
	'jquery',
	'underscore',
	'backbone',
	'marionette',
	'mustache',

	'app/app'
], function ($, _, Backbone, Marionette, Mustache, App) {
	'use strict';

	var mainApp;
	//Override Marionette default renderer to use Mustache to render templates
	Backbone.Marionette.Renderer.render = function (template, data) {
		return template(data);
	};

	//Fetch the cookieData (mocked locally for testing purposes)
	mainApp = new App();
	mainApp.start();
});
