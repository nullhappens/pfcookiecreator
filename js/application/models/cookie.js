/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	'use strict';

	var Cookie = Backbone.Model.extend({});
	return Cookie;
});
