/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	'use strict';
	var CookieData = Backbone.Collection.extend({});
	return CookieData;
});
