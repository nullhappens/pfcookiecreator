/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/rootView'
], function ($, _, Backbone, Marionette, RootView) {
	'use strict';
	var App = Marionette.Application.extend({
		rootView: null,
		onStart: function () {
			this.rootView = new RootView();
			this.rootView.render();
		}
	});

	return App;
});
