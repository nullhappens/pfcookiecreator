/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieWizard',
	'views/cookieName',
	'views/cookieRegister',
	'models/cookieData'
], function ($, _, Backbone, Marionette, CookieWizardView, CookieNameView, CookieRegisterView, CookieData) {
	'use strict';
	var cookieWizardView,
		cookieNameView,
		cookieRegisterView,
		cookie,
		cookieData,
		RootView = Marionette.ItemView.extend({
			el: '#main',
			template: false,

			ui: {
				step1: '.js_step1',
				step2: '.js_step2',
				step3: '.js_step3',
				step1Content: '.step1 .content',
				step2Content: '.step2 .content',
				step3Content: '.step3 .content',
				wizardEnd: '.wizardEnd'
			},

			onRender: function () {

				//TODO: use this only for testing purposes
				// cookieWizardView = new CookieWizardView();
				// cookieWizardView.cookie = new Backbone.Model();
				// $('.js_step1').hide();
				// this.onWizardStep3();
				// return;

				var onFetchSuccess;
				if (!cookieData) {
					onFetchSuccess = function (result) {
						cookieWizardView = new CookieWizardView({cookieData: result});
						this.listenTo(cookieWizardView, 'wizard:nextStep', this.onWizardStep2);
						cookieWizardView.render();
					}.bind(this);
					cookieData = new CookieData();
					//TODO: This should ideally come from an actual server API
					cookieData.fetch({url: '/data/cookieData.txt', success: onFetchSuccess});
				}
			},

			onWizardStep2: function () {
				this.transitionStep('step1', 'step2');
				cookie = cookieWizardView.cookie;
				cookieNameView = new CookieNameView({model: cookie});
				this.listenTo(cookieNameView, 'wizard:nextStep', this.onWizardStep3);
				cookieNameView.render();
			},

			onWizardStep3: function () {
				this.transitionStep('step2', 'step3');
				cookie = cookieWizardView.cookie;
				cookieRegisterView = new CookieRegisterView({model: cookie});
				this.listenTo(cookieRegisterView, 'wizard:nextStep', this.onWizardFinish);
				cookieRegisterView.render();
			},

			onWizardFinish: function () {
				this.ui.wizardEnd.slideDown(400);
			},

			transitionStep: function (from, to) {
				this.ui[from + 'Content'].slideUp(400, function () {
					this.ui[from].removeClass('checked');
					this.ui[to].addClass('checked');
					this.ui[to + 'Content'].slideDown(400);
				}.bind(this));
				this.ui[to].focus();
			}
		});
	return RootView;
});
