/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'stache!templates/cookieResult'
], function ($, _, Backbone, Marionette, cookieResultTemplate) {
	'use strict';
	var CookieResultView = Marionette.ItemView.extend({
		cookie: null,
		template: cookieResultTemplate,
		className: 'js_cookieResultView',

		ui: {
			previousButton: '.js_previousButton',
			btnDone: '.js_btnDone',
			cookieToppings: '.js_cookieToppings',
			cookieTop: '.js_cookieTop'
		},

		triggers: {
			'click @ui.btnDone': 'cookieResult:done',
			'click @ui.previousButton': 'cookieResult:previous'
		},

		initialize: function (options) {
			this.cookie = options.cookie;
		},

		onRender: function () {
			if (this.cookie.toppings.length === 0) {
				this.ui.cookieToppings.hide();
			}
			if (this.cookie.get('cookieType') === 'classic-milano') {
				if (this.cookie.fillings.length > 1) {
					this.ui.cookieTop.removeClass('cookieTop1');
					this.ui.cookieTop.addClass('cookieTop2');
				}
				this.ui.cookieTop.removeClass('hide');
			}
		},

		serializeData: function () {
			var cookieFillings = [],
				cookieToppings = [];

			this.cookie.fillings.each(function (filling, index) {
				var className = 'filling' + index;
				if (this.cookie.get('cookieType') === 'milano-melts') {
					className = 'meltFilling';
				}
				cookieFillings.push(_.extend(filling.attributes, {className: className}));
			}.bind(this));

			if (this.cookie.toppings) {
				this.cookie.toppings.each(function (topping, index) {
					var className = 'topping' + index;
					cookieToppings.push(_.extend(topping.attributes, {className: className}));
				});
			}

			return {
				cookieTypeName: this.cookie.get('cookieName'),
				cookieFlavor: this.cookie.flavor.get('name'),
				cookieImageTop: this.cookie.flavor.get('imageTop'),
				cookieImage: this.cookie.flavor.get('image'),
				cookieFillings: cookieFillings,
				cookieToppings: cookieToppings,
				imageBase: '/img/cookieImages'
			};
		}

	});
	return CookieResultView;
});
