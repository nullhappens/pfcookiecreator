[{
	"id": "classic-milano",
	"name": "Classic Milano<sup>&reg;</sup>",
	"image": "",
	"flavors": [
		{"id": "vanilla","name": "Vanilla", "imageTop": "cookieFlavor_clasicVanilla.png","image": "cookieFlavor_slicesVanilla.png"},
		{"id": "chocolate","name": "Chocolate", "imageTop": "cookieFlavor_clasicChocolate.png","image": "cookieFlavor_slicesChocolate.png"},
		{"id": "vanilla-chocolate","name": "Vanilla &amp; Chocolate", "imageTop": "cookieFlavor_clasicVanilla.png","image": "cookieFlavor_slicesChocolate.png"},
		{"id": "ginger","name": "Ginger", "imageTop": "cookieFlavor_clasicGinger.png","image": "cookieFlavor_slicesGinger.png"},
		{"id": "graham","name": "Graham", "imageTop": "cookieFlavor_clasicGraham.png","image": "cookieFlavor_slicesGraham.png"},
		{"id": "vanilla-almond","name": "Vanilla Almond", "imageTop": "cookieFlavor_clasicVanillaAlmond.png","image": "cookieFlavor_slicesVanillaAlmond.png"},
		{"id": "coffee-chocolate","name": "Coffee Chocolate", "imageTop": "cookieFlavor_clasicChocolate.png","image": "cookieFlavor_slicesChocolate.png"},
		{"id": "chocolate-chip","name": "Chocolate Chip", "imageTop": "cookieFlavor_clasicChocolateChip.png","image": "cookieFlavor_slicesChocolateChip.png"}
	],
	"fillings": {
		"chocolate": [
			{"id": "white-chocolate", "name": "White Chocolate", "image": "cookieCreme_WhiteChocolate.png"},
			{"id": "milk-chocolate", "name": "Milk Chocolate", "image": "cookieCreme_MilkChocolate.png"},
			{"id": "dark-chocolate", "name": "Dark Chocolate", "image": "cookieCreme_DarkChocolate.png"},
			{"id": "double-dark-chocolate", "name": "Double Dark Chocolate", "image": "cookieCreme_DoubleDarkChocolate.png"},
			{"id": "double-milk-chocolate", "name": "Double Milk Chocolate", "image": "cookieCreme_DoubleMilkChocolate.png"},
			{"id": "hazelnut-chocolate", "name": "Hazelnut Chocolate", "image": "cookieCreme_HazelnutChocolate.png"},
			{"id": "coffee-chocolate", "name": "Coffee Chocolate", "image": "cookieCreme_CoffeeChocolate.png"}
		],
		"fruit": [
			{"id": "strawberry", "name": "Strawberry", "image": "cookieCreme_Strawberry.png"},
			{"id": "mango", "name": "Mango", "image": "cookieCreme_Mango.png"},
			{"id": "lemon", "name": "Lemon", "image": "cookieCreme_Lemon.png"},
			{"id": "lime", "name": "Lime", "image": "cookieCreme_Lime.png"},
			{"id": "coconut", "name": "Coconut", "image": "cookieCreme_Coconut.png"},
			{"id": "mint", "name": "Mint", "image": "cookieCreme_Mint.png"}
		],
		"other": [
			{"id": "coffee", "name": "Coffee", "image": "cookieCreme_Coffee.png"},
			{"id": "hazelnut", "name": "Hazelnut", "image": "cookieCreme_Hazelnut.png"},
			{"id": "caramel", "name": "Caramel", "image": "cookieCreme_Caramel.png"},
			{"id": "salted-caramel", "name": "Salted Caramel", "image": "cookieCreme_SaltedCacramel.png"},
			{"id": "maple", "name": "Maple", "image": "cookieCreme_Maple.png"},
			{"id": "marshmallow", "name": "Marshmallow", "image": "cookieCreme_Marshmallow.png"},
			{"id": "butterscotch", "name": "Butterscotch", "image": "cookieCreme_Butterscotch.png"},
			{"id": "molasses", "name": "Molasses", "image": "cookieCreme_Molasses.png"},
			{"id": "creme-brulee", "name": "Cr&eacute;me Brulee", "image": "cookieCreme_CremeBrulee.png"},
			{"id": "pumpkin-spice", "name": "Pumpkin Spice", "image": "cookieCreme_PumpkinSpice.png"}
		]
	}
},{
	"id": "milano-slices",
	"name": "Milano Slices<sup>&#8482;</sup>",
	"image": "",
	"flavors": [
		{"id": "vanilla","name": "Vanilla", "image": "cookieFlavor_slicesVanilla.png"},
		{"id": "chocolate","name": "Chocolate", "image": "cookieFlavor_slicesChocolate.png"},
		{"id": "ginger","name": "Ginger", "image": "cookieFlavor_slicesGinger.png"},
		{"id": "graham","name": "Graham", "image": "cookieFlavor_slicesGraham.png"},
		{"id": "vanilla-almond","name": "Vanilla Almond", "image": "cookieFlavor_slicesVanillaAlmond.png"},
		{"id": "coffee-chocolate","name": "Coffee Chocolate", "image": "cookieFlavor_slicesChocolate.png"},
		{"id": "chocolate-chip","name": "Chocolate Chip", "image": "cookieFlavor_slicesChocolateChip.png"}
	],
	"fillings": {
		"chocolate": [
			{"id": "white-chocolate", "name": "White Chocolate", "image": "cookieCreme_WhiteChocolate.png"},
			{"id": "milk-chocolate", "name": "Milk Chocolate", "image": "cookieCreme_MilkChocolate.png"},
			{"id": "dark-chocolate", "name": "Dark Chocolate", "image": "cookieCreme_DarkChocolate.png"},
			{"id": "double-dark-chocolate", "name": "Double Dark Chocolate", "image": "cookieCreme_DoubleDarkChocolate.png"},
			{"id": "double-milk-chocolate", "name": "Double Milk Chocolate", "image": "cookieCreme_DoubleMilkChocolate.png"},
			{"id": "hazelnut-chocolate", "name": "Hazelnut Chocolate", "image": "cookieCreme_HazelnutChocolate.png"},
			{"id": "coffee-chocolate", "name": "Coffee Chocolate", "image": "cookieCreme_CoffeeChocolate.png"}
		],
		"fruit": [
			{"id": "strawberry", "name": "Strawberry", "image": "cookieCreme_Strawberry.png"},
			{"id": "mango", "name": "Mango", "image": "cookieCreme_Mango.png"},
			{"id": "lemon", "name": "Lemon", "image": "cookieCreme_Lemon.png"},
			{"id": "lime", "name": "Lime", "image": "cookieCreme_Lime.png"},
			{"id": "coconut", "name": "Coconut", "image": "cookieCreme_Coconut.png"},
			{"id": "mint", "name": "Mint", "image": "cookieCreme_Mint.png"}
		],
		"other": [
			{"id": "coffee", "name": "Coffee", "image": "cookieCreme_Coffee.png"},
			{"id": "hazelnut", "name": "Hazelnut", "image": "cookieCreme_Hazelnut.png"},
			{"id": "caramel", "name": "Caramel", "image": "cookieCreme_Caramel.png"},
			{"id": "salted-caramel", "name": "Salted Caramel", "image": "cookieCreme_SaltedCacramel.png"},
			{"id": "maple", "name": "Maple", "image": "cookieCreme_Maple.png"},
			{"id": "marshmallow", "name": "Marshmallow", "image": "cookieCreme_Marshmallow.png"},
			{"id": "butterscotch", "name": "Butterscotch", "image": "cookieCreme_Butterscotch.png"},
			{"id": "molasses", "name": "Molasses", "image": "cookieCreme_Molasses.png"},
			{"id": "creme-brulee", "name": "Cr&eacute;me Brulee", "image": "cookieCreme_CremeBrulee.png"},
			{"id": "pumpkin-spice", "name": "Pumpkin Spice", "image": "cookieCreme_PumpkinSpice.png"}
		]
	},
	"toppings": {
		"candy": [
			{"id": "chocolate-covered-crisp", "name": "Chocolate Covered Crisp", "image": "toppings_ChocolateCoveredCrisp.png"},
			{"id": "chocolate-covered-pretzels", "name": "Chocolate Covered Pretzels", "image": "toppings_ChocolateCoveredPretzels.png"},
			{"id": "cocoa-nibs", "name": "Cocoa Nibs", "image": "toppings_CocoaNibs.png"},
			{"id": "crushed-graham-crackers", "name": "Graham Crackers", "image": "toppings_CrushedGrahamCrackers.png"},
			{"id": "crushed-malted-milk-balls", "name": "Malted Milk Balls", "image": "toppings_CrushedMaltedMilkBalls.png"},
			{"id": "crushed-peppermint-candy", "name": "Peppermint Candy", "image": "toppings_CrushedPeppermintCandy.png"},
			{"id": "crushed-potato-chips", "name": "Potato Chips", "image": "toppings_CrushedPotatoChips.png"},
			{"id": "rock-candy", "name": "Rock Candy", "image": "toppings_RockCandy.png"},
			{"id": "toffee", "name": "Toffee", "image": "toppings_Toffee.png"},
			{"id": "white-chocolate-chips", "name": "White Chocolate Chips", "image": "toppings_WhiteChocolateChips.png"}
		],
		"fruit": [
			{"id": "candied-apple", "name": "Candied Apple", "image": "toppings_CandiedApple.png"},
			{"id": "candied-ginger", "name": "Candied Ginger", "image": "toppings_CandiedGinger.png"},
			{"id": "dried-apricots", "name": "Dried Apricots", "image": "toppings_DriedApricots.png"},
			{"id": "dried-cranberries", "name": "Dried Cranberries", "image": "toppings_DriedCranberries.png"},
			{"id": "dried-pineapple", "name": "Dried Pineapple", "image": "toppings_DriedPineapple.png"},
			{"id": "lemon-peel", "name": "Lemon Peel", "image": "toppings_LemonPeel.png"},
			{"id": "orange-peel", "name": "Orange Peel", "image": "toppings_OrangePeel.png"},
			{"id": "raisins", "name": "Raisins", "image": "toppings_Raisins.png"}
		],
		"nuts": [
			{"id": "cashews", "name": "Cashews", "image": "toppings_Cashews.png"},
			{"id": "macadamias", "name": "Macadamias", "image": "toppings_Macadamias.png"},
			{"id": "pecans", "name": "Pecans", "image": "toppings_Pecans.png"},
			{"id": "pistachios", "name": "Pistachios", "image": "toppings_Pistachios.png"},
			{"id": "almonds", "name": "Almonds", "image": "toppings_Almonds.png"},
			{"id": "sunflower-seeds", "name": "Sunflower Seeds", "image": "toppings_SunflowerSeeds.png"}
		],
		"spices": [
			{"id": "candied-lavender", "name": "Candied lavender", "image": "toppings_CandiedLavender.png"},
			{"id": "chilli-powder", "name": "Chili Powder", "image": "toppings_ChiliPowder.png"},
			{"id": "cinnamon", "name": "Cinnamon", "image": "toppings_Cinnamon.png"},
			{"id": "coconut", "name": "Coconut", "image": "toppings_Coconut.png"},
			{"id": "sea-salt", "name": "Sea Salt", "image": "toppings_SeaSalt.png"}

		]
	}
},
{
	"id": "milano-melts",
	"name": "Milano Melts<sup>&reg;</sup>",
	"image": "",
	"flavors": [
		{"id": "vanilla","name": "Vanilla", "image": "cookieFlavor_meltsVanilla.png"},
		{"id": "chocolate","name": "Chocolate", "image": "cookieFlavor_meltsChocolate.png"},
		{"id": "ginger","name": "Ginger", "image": "cookieFlavor_meltsGinger.png"},
		{"id": "graham","name": "Graham", "image": "cookieFlavor_meltsGraham.png"},
		{"id": "vanilla-almond","name": "Vanilla Almond", "image": "cookieFlavor_meltsVanillaAlmond.png"},
		{"id": "coffee-chocolate","name": "Coffee Chocolate", "image": "cookieFlavor_meltsChocolate.png"},
		{"id": "chocolate-chip","name": "Chocolate Chip", "image": "cookieFlavor_meltsChocolateChip.png"}
	],
	"fillings": {
		"chocolate": [
			{"id": "white-chocolate", "name": "White Chocolate", "image": "cookieCremeMelts_WhiteChocolate.png"},
			{"id": "milk-chocolate", "name": "Milk Chocolate", "image": "cookieCremeMelts_MilkChocolate.png"},
			{"id": "dark-chocolate", "name": "Dark Chocolate", "image": "cookieCremeMelts_DarkChocolate.png"},
			{"id": "double-dark-chocolate", "name": "Double Dark Chocolate", "image": "cookieCremeMelts_DoubleDarkChocolate.png"},
			{"id": "double-milk-chocolate", "name": "Double Milk Chocolate", "image": "cookieCremeMelts_DoubleMilkChocolate.png"},
			{"id": "hazelnut-chocolate", "name": "Hazelnut Chocolate", "image": "cookieCremeMelts_HazelnutChocolate.png"},
			{"id": "coffee-chocolate", "name": "Coffee Chocolate", "image": "cookieCremeMelts_CoffeeChocolate.png"}
		],
		"fruit": [
			{"id": "strawberry", "name": "Strawberry", "image": "cookieCremeMelts_Strawberry.png"},
			{"id": "mango", "name": "Mango", "image": "cookieCremeMelts_Mango.png"},
			{"id": "lemon", "name": "Lemon", "image": "cookieCremeMelts_Lemon.png"},
			{"id": "lime", "name": "Lime", "image": "cookieCremeMelts_Lime.png"},
			{"id": "coconut", "name": "Coconut", "image": "cookieCremeMelts_Coconut.png"},
			{"id": "mint", "name": "Mint", "image": "cookieCremeMelts_Mint.png"}
		],
		"other": [
			{"id": "coffee", "name": "Coffee", "image": "cookieCremeMelts_Coffee.png"},
			{"id": "hazelnut", "name": "Hazelnut", "image": "cookieCremeMelts_Hazelnut.png"},
			{"id": "maple", "name": "Maple", "image": "cookieCremeMelts_Maple.png"},
			{"id": "marshmallow", "name": "Marshmallow", "image": "cookieCremeMelts_Marshmallow.png"},
			{"id": "butterscotch", "name": "Butterscotch", "image": "cookieCremeMelts_Butterscotch.png"},
			{"id": "molasses", "name": "Molasses", "image": "cookieCreme_Molasses.png"},
			{"id": "creme-brulee", "name": "Cr&eacute;me Brulee", "image": "cookieCremeMelts_CremeBrulee.png"},
			{"id": "pumpkin-spice", "name": "Pumpkin Spice", "image": "cookieCremeMelts_PumpkinSpice.png"}
		]
	}
}]
