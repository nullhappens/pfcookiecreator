/*globals module*/
module.exports = function (grunt) {
	'use strict';
	grunt.initConfig({
		jshint: {
			options: {
				jshintrc: true,
				force: true
			},
			all: ['Gruntfile.js', 'js/application/**/*.js', 'js/main.js']
		},

		watch: {
			scripts: {
				files: ['js/**/*.js'],
				tasks: ['jshint', 'requirejs:dev']
			}
		},

		connect: {
			options: {
				base: './',
				port: 3000,
				debug: true
			},
			dev: {},
			prod: {
				options: {
					keepalive: true
				}
			}
		},

		clean: {
			dist: [
				'dist/js/*',
				'!dist/js/require.js'
			]
		},

		requirejs: {
			options: {
				mainConfigFile: 'js/main.js'
			},

			dist: {
				options: {
					baseUrl: './js',
					optimize: 'uglify2',
					name: 'main',
					out: 'dist/js/main.js',
					generateSourceMaps: true,
					preserveLicenseComments: false
				}
			},

			dev: {
				options: {
					keepBuildDir: true,
					appDir: './js',
					baseUrl: './',
					dir: 'dist/js',
					optimize: 'none'
				}
			}
		}
	});

	grunt.registerTask('default', ['connect:dev', 'clean', 'requirejs:dev', 'watch']);
	grunt.registerTask('heroku', ['clean', 'requirejs:dist']);
	grunt.registerTask('prod', ['clean', 'requirejs:dist', 'connect:prod']);
	grunt.registerTask('dist', ['clean', 'requirejs:dist']);

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-clean');

};
